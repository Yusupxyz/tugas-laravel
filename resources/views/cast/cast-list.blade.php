@extends('layouts.master')
@push('style')
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush

@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

<div class="card m-2">
    <div class="card-header">
      <h3 class="card-title">Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('success'))
          <div class="alert alert-success alert-dismissible fade show">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      @endif
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr class="text-center">
          <th>No.</th>
          <th>Nama</th>
          <th>Umur (Tahun)</th>
          <th>Bio</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($casts as $item => $value)
            <tr>
              <td>{{ $item+1 }}</td>
              <td>{{ $value->nama }}</td>
              <td class="text-center">{{ $value->umur }}</td>
              <td>{!! substr($value->bio,0,7) !!}...</td>
              <td class="d-flex justify-content-center" style="display:flex">
                <a href="{{ route('cast.show',['cast' => $value->id ])}}" class="btn btn-info btn-sm mr-2">Detail</a>
                <a href="{{ route('cast.edit',['cast' => $value->id ])}}" class="btn btn-secondary btn-sm mr-2">Ubah</a>
                <form role="form" action="{{ route('cast.destroy',['cast' => $value->id ])}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
          @endforeach
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('script') 
    <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush