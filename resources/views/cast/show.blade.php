@extends('layouts.master')


@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail Cast</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <div class="card m-2">
    <div class="card-body row">
      <div class="col-5 text-center d-flex align-items-center justify-content-center">
        <div class="">
          <h2><strong>{{ $cast->nama }}</strong></h2>
          <p class="lead mb-5">{{ $cast->umur }} Tahun
          </p>
        </div>
      </div>
      <div class="col-7">
        <div class="form-group">
          <label for="inputName">Bio:</label>
          <h6 class="border"><i>{!! $cast->bio !!}</i></h6>
        </div>
        <div class="form-group">
          <a href="{{ route('cast.index') }}" role="button" class="btn btn-primary btn-sm" >Kembali</a>
        </div>
      </div>
    </div>
  </div>

@endsection
