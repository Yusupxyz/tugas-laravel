@extends('layouts.master')
@push('style')
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('/adminlte/plugins/summernote/summernote-bs4.min.css')}}">
@endpush

@section('content')
 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <div class="card card-secondary m-2">
    <div class="card-header">
      <h3 class="card-title">Ubah Data Cast {{ $cast->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('cast.update',['cast' => $cast->id ])}}" method="POST">
      @method('PUT')
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="exampleInputNama">Nama</label>
          <input type="text" class="form-control" name="nama" value="{{old('nama',$cast->nama)}}" placeholder="Ketikan Nama" >
          @error('nama')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputUmur">Umur (Tahun)</label>
          <input type="number" class="form-control" name="umur" value="{{old('umur',$cast->umur)}}" placeholder="Ketikan Umur" >
          @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputBio">Bio</label>
            <textarea id="summernote" name="bio" >{{old('bio',$cast->bio)}}</textarea>
            @error('bio')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-secondary">Ubah</button>
      </div>
    </form>
  </div>  
@endsection

@push('script') 
    <!-- Summernote -->
    <script src="{{asset('/adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
    $(function () {
    // Summernote
      $('#summernote').summernote()
    })
    </script>
@endpush