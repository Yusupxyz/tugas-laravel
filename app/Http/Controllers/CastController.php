<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $casts = Cast::all();
        return view('cast.cast-list', compact('casts'));
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        Cast::create([
    		"nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
    	]);
    
        return redirect('/cast')->with('success','Data Cast berhasil tersimpan!');
    }

    public function show($id){
        $cast = Cast::find($id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = Cast::find($id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id,Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast')->with('success','Data Cast berhasil diubah!');
    }

    
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast')->with('success','Data Cast berhasil dihapus!');
    }
}
