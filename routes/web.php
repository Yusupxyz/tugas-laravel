<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('layouts.master');
});

Route::get('/table', function () {
    return view('tugas.table');
});

Route::get('/data-tables', function () {
    return view('tugas.data-tables');
});

Route::resource('cast','CastController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
